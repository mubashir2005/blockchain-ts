const lightningHash= (data)=>{
    return data+ "*"
}
class Block{
    data:string;
    hash:string;
    lastHash:string;

    constructor(data,hash,lastHash){
        this.data= data;
        this.hash= hash;
        this.lastHash=lastHash;
    }
}

class BlockChain{
    chain: Array<any>
    constructor(){
        const genesis = new Block('gen-data','gen-hash','gen-lastHash')
        this.chain=[genesis]
    }
    addBlock(data){
        const lastHash= this.chain[this.chain.length-1].hash
        const hash= lightningHash(data+ lastHash)
        const block = new Block(data,hash,lastHash)
        this.chain.push(block)
    }
}
const foodBlockhain= new BlockChain();
foodBlockhain.addBlock('one')
foodBlockhain.addBlock('two')
foodBlockhain.addBlock('three')

console.log(foodBlockhain)
